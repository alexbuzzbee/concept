package concept

import (
	"errors"
	"os"
	"plugin"
)

// ComponentKind indicates the type of a component.
type ComponentKind int

const (
	// ApplicationComponent is the kind of an application providing HTTP endpoints.
	ApplicationComponent = iota
	// ExtensionComponent is the kind of an extension installing hooks into applications and the system.
	ExtensionComponent = iota
)

// A Component provides new functionality to Concept.
type Component struct {
	// The name identifies the component. It should be a fully-qualified Go package name (e.g. gitlab.com/alexbuzzbee/concept-admin) followed by a period and the unqualified name (e.g. App).
	Name     string
	Kind     ComponentKind
	Exported []interface{}
}

// An ExtensionHook is a hook exported by an extension.
type ExtensionHook struct {
	Name string
	F    HookFunc
}

// Installs a component by performing all needed registrations etc.
func install(comp Component) (err error) {
	defer func() {
		if p := recover(); p != nil {
			err = p.(error)
		}
	}()
	switch comp.Kind {
	case ApplicationComponent:
		for _, endpoint := range comp.Exported {
			if err = CreateEndpoint(endpoint.(Endpoint).Path, endpoint.(Endpoint).Handler); err != nil {
				return
			}
		}
	case ExtensionComponent:
		for _, hook := range comp.Exported {
			if err = AddHook(hook.(ExtensionHook).Name, hook.(ExtensionHook).F); err != nil {
				return
			}
		}
	default:
		installers, err := CallHook("ComponentKind", comp.Kind)
		if err != nil {
			return err
		}
		success := false
		for _, installer := range installers {
			if installer != nil {
				if err = installer.(func(Component) error)(comp); err != nil {
					return err
				}
				success = true
				break
			}
		}
		if !success {
			return errors.New("no such component kind: " + string(comp.Kind))
		}
	}
	return nil
}

// Load loads a Concept plugin and installs its components.
func Load(name string) (err error) {
	defer func() {
		if p := recover(); p != nil {
			err = p.(error)
		}
	}()
	wd, err := os.Getwd()
	if err != nil {
		return
	}
	loaded, err := plugin.Open(wd + "/" + name + ".so")
	if err != nil {
		return
	}
	Activate, err := loaded.Lookup("Activate")
	defs := Activate.(func() []Component)()
	for _, comp := range defs {
		err = install(comp)
		if err != nil {
			return
		}
	}
	return nil
}

func init() {
	NewHook("ComponentKind")
}
