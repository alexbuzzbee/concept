package concept

import (
	"bytes"
	"html/template"
)

type layout struct {
	tmpl   template.Template
	parent *layout
}

var layouts map[string]*layout

// runLayoutDirect executes the underlying templates of a layout and all its parents.
func runLayoutDirect(layout *layout, content template.HTML, context map[string]interface{}) (result string, err error) {
	sw := new(bytes.Buffer)
	savedCtxContent := context["content"]
	context["content"] = content
	defer func() { context["content"] = savedCtxContent }()
	if err = layout.tmpl.Execute(sw, context); err != nil {
		return "", err
	}
	if parent := layout.parent; parent != nil {
		return runLayoutDirect(parent, template.HTML(sw.Bytes()), context)
	}
	return string(sw.Bytes()), nil
}

// RunLayout executes a layout, wrapping it in its parent layout(s), if any.
func RunLayout(name string, content template.HTML, context map[string]interface{}) (result string, err error) {
	return runLayoutDirect(layouts[name], content, context)
}

// CreateLayout creates and registers a layout.
// A layout's name should be a fully-qualified Go package name (e.g. gitlab.com/alexbuzzbee/concept-admin) followed by a period and the unqualified name (e.g. ConfigPage).
func CreateLayout(name string, tmpl template.Template, parent string) {
	if layouts == nil {
		layouts = make(map[string]*layout)
	}
	layout := new(layout)
	layout.tmpl = tmpl
	if parent != "" {
		layout.parent = layouts[parent]
	}
	layouts[name] = layout
}

func init() {
	if layouts == nil {
		layouts = make(map[string]*layout)
	}
	skelText := `<!DOCTYPE html>
<html>
  <head>
    <meta charset='UTF-8' />
    <meta name='viewport' content='width=device-width, initial-scale=1.0' />
    <meta name='keywords' content='{{ .page.tags }}' />
    <meta name='description' content='{{ .page.desc }}' />
    <meta name='author' content='{{ .page.author }}' />
    <title>{{ .page.title }}</title>
  </head>
  <body>
    {{ .content }}
  </body>
</html>
`
	skel := template.New("gitlab.com/alexbuzzbee/concept.Skeleton")
	skel, err := skel.Parse(skelText)
	if err != nil {
		panic(err)
	}
	CreateLayout("gitlab.com/alexbuzzbee/concept.Skeleton", *skel, "")
}
