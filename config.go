package concept

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"strings"
)

var conf map[string]interface{}

func findKey(key string) (table map[string]interface{}, name string, err error) {
	names := strings.Split(key, ":")
	table = conf
	var i int
	for i, name = range names {
		if table[name] == nil {
			return table, name, errors.New("no such key: " + key)
		}
		switch v := table[name].(type) {
		case map[string]interface{}:
			table = v
		default:
			if i < len(names)-1 {
				return table, "", errors.New("no such key: " + key)
			}
		}
	}
	return
}

// GetConf reads a configuration key. Returns def (default) if value not found.
func GetConf(key string, def interface{}) (value interface{}, err error) {
	table, name, err := findKey(key)
	if err != nil {
		return def, err
	}
	return table[name], nil
}

func init() {
	buf, err := ioutil.ReadFile("config.json")
	if err != nil {
		panic(err)
	}
	conf = make(map[string]interface{})
	err = json.Unmarshal(buf, &conf)
	if err != nil {
		panic(err)
	}
}
