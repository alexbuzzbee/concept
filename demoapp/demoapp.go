// A plugin for Concept that provides a basic demonstration application.
package main

import (
	"errors"

	"gitlab.com/alexbuzzbee/concept"
)

// Index is the handler for /.
func Index(path string, params map[string][]string) (content string, extra concept.ResponseData, err error) {
	content, err = concept.Render("gitlab.com/alexbuzzbee/concept.Skeleton", "<h1>Demo application</h1>\n<p>This is a demo page from a Concept application.</p>", "Demo application", "Demo", "Concept", "A demo Concept application.", nil)
	if err != nil {
		extra = concept.ResponseData{ResponseCode: 500}
	} else {
		extra = concept.ResponseData{ResponseCode: 200}
	}
	return
}

// Page is the handler for /demoapp/*.
func Page(path string, params map[string][]string) (content string, extra concept.ResponseData, err error) {
	if params["crash"] != nil {
		panic(errors.New("this is a test error"))
	}
	content, err = concept.Render("gitlab.com/alexbuzzbee/concept.Skeleton", "<h1>Demo application</h1>\n<p>This is a demo page from a Concept application.</p><p>You asked for: <code>"+path+"</code></p>", "Demo application", "Demo", "Concept", "A demo Concept application.", nil)
	if err != nil {
		extra = concept.ResponseData{ResponseCode: 500}
	} else {
		extra = concept.ResponseData{ResponseCode: 200}
	}
	return
}

// Activate is the activation function for loading.
func Activate() (comps []concept.Component) {
	path, _ := concept.GetConf("gitlab.com/alexbuzzbee/concept/demoapp:path", "/demoapp/")
	handleRoot, _ := concept.GetConf("gitlab.com/alexbuzzbee/concept/demoapp:handleRoot", false)
	var exports []interface{}
	if handleRoot.(bool) {
		exports = []interface{}{
			interface{}(concept.Endpoint{
				Path:    "/",
				Handler: concept.EndpointHandler(Index),
			}),
			interface{}(concept.Endpoint{
				Path:    path.(string),
				Handler: concept.EndpointHandler(Page),
			}),
		}
	} else {
		exports = []interface{}{
			interface{}(concept.Endpoint{
				Path:    path.(string),
				Handler: concept.EndpointHandler(Page),
			}),
		}
	}
	comps = make([]concept.Component, 1)
	comps[0] = concept.Component{
		Name:     "gitlab.com/alexbuzzbee/concept/demoapp.App",
		Kind:     concept.ApplicationComponent,
		Exported: exports,
	}
	return
}
