#!/bin/sh
go vet $(go list ../...)
go build -race
go build -buildmode plugin -race gitlab.com/alexbuzzbee/concept/demoapp