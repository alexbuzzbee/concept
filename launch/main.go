package main

import (
	"os"

	"gitlab.com/alexbuzzbee/concept"
)

func main() {
	val, err := concept.GetConf("plugins", []string{})
	plugins := val.([]interface{})
	if val == nil {
		println("Init error: " + err.Error())
		os.Exit(1)
	}
	for _, name := range plugins {
		err := concept.Load(name.(string))
		if err != nil {
			println("Load error: ", err.Error())
			os.Exit(1)
		}
	}

	val, err = concept.GetConf("host", ":80")
	if val == nil {
		println("Serve config error: " + err.Error())
		os.Exit(1)
	}
	println("Serve error: " + concept.Serve(val.(string)).Error())
}
