package concept

import (
	"errors"
	"html/template"

	"github.com/ryanuber/go-glob"
)

// An EndpointHandler is a function that handles endpoints.
type EndpointHandler func(path string, params map[string][]string) (content string, extra ResponseData, err error)

// An Endpoint is an actual endpoint.
type Endpoint struct {
	// The Path is the endpoint's path; it may contain wildcards.
	Path    string
	Handler EndpointHandler
}

// A ResponseData contains extra information about a dispatch result besides its content and/or error.
type ResponseData struct {
	ResponseCode     int
	RedirectLocation string
}

var endpoints map[string]EndpointHandler

// Errors contains the server's built-in error pages.
var Errors = map[int]string{
	404: "gitlab.com/alexbuzzbee/concept.Error404",
	500: "gitlab.com/alexbuzzbee/concept.Error500",
}

// CreateEndpoint creates and sets up an endpoint.
func CreateEndpoint(path string, handler EndpointHandler) error {
	if endpoints[path] != nil {
		return errors.New("endpoint already exists")
	}
	endpoints[path] = handler
	return nil
}

// DispatchError renders an error page.
func DispatchError(path string, code int, err error) (content string, extra ResponseData, _err error) {
	content, e := RunLayout(Errors[code], template.HTML(""), map[string]interface{}{"path": path, "err": err})
	if e != nil {
		return "<p>An error with HTTP code " + string(code) + " occured. Additionally, an error occured while rendering the error report page.</p>", ResponseData{ResponseCode: code}, err
	}
	return content, ResponseData{ResponseCode: code}, err
}

// DispatchEndpoint calls the correct endpoint handler for the specified path.
func DispatchEndpoint(requestedPath string, params map[string][]string) (content string, extra ResponseData, err error) {
	defer func() {
		if e := recover(); e != nil {
			content, extra, err = DispatchError(requestedPath, 500, e.(error))
		}
	}()
	var handler EndpointHandler
	specificity := 0 // How long the most specific path so far is.
	for patt, theHandler := range endpoints {
		if glob.Glob(patt, requestedPath) {
			if len(patt) > specificity {
				specificity = len(patt)
				handler = theHandler
			}
		}
	}
	if handler == nil {
		return DispatchError(requestedPath, 404, errors.New("no such endpoint: "+requestedPath))
	}
	return handler(requestedPath, params)
}

func init() {
	endpoints = make(map[string]EndpointHandler)

	e404 := template.New("gitlab.com/alexbuzzbee/concept.Error404")
	e404, err := e404.Parse("<!DOCTYPE html>\n<html>\n<head>\n<title>Not found</title>\n</head>\n<body>\n<h1>Error</h1>\n<p>Error 404: Not found.</p>\n<p>The page <code>{{ .path }}</code> was not found on this server.</p>\n</body>\n</html>")
	if err != nil {
		panic(err)
	}
	CreateLayout("gitlab.com/alexbuzzbee/concept.Error404", *e404, "")

	e500 := template.New("gitlab.com/alexbuzzbee/concept.Error500")
	e500, err = e500.Parse("<!DOCTYPE html>\n<html>\n<head>\n<title>Internal server error</title>\n</head>\n<body>\n<h1>Error</h1>\n<p>Error 500: Internal server error.</p>\n<p>The page <code>{{ .path }}</code> could not be displayed due to an internal error:\n<br />\n<code>{{ .err }}</code></p>\n</body>\n</html>")
	if err != nil {
		panic(err)
	}
	CreateLayout("gitlab.com/alexbuzzbee/concept.Error500", *e500, "")
}
