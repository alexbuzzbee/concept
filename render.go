package concept

import (
	"html/template"
)

// Render renders a page using a layout with a title, author, description, some tags, and optionally extra data.
func Render(layout, content, title, tags, author, desc string, extra map[string]interface{}) (result string, err error) {
	context := make(map[string]interface{})
	if extra != nil {
		for i, v := range extra {
			context[i] = v
		}
	}
	context["page"] = make(map[string]interface{})
	context["page"].(map[string]interface{})["title"] = title
	context["page"].(map[string]interface{})["tags"] = tags
	context["page"].(map[string]interface{})["author"] = author
	context["page"].(map[string]interface{})["desc"] = desc
	contentHtml := template.HTML(content)
	return RunLayout(layout, contentHtml, context)
}
