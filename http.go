package concept

import (
	"net/http"
)

type handler struct{}

func (*handler) ServeHTTP(writer http.ResponseWriter, req *http.Request) {
	content, extra, err := DispatchEndpoint(req.URL.Path, map[string][]string(req.URL.Query()))
	writer.WriteHeader(extra.ResponseCode)
	if extra.ResponseCode >= 300 && extra.ResponseCode <= 399 {
		writer.Header().Set("Location", extra.RedirectLocation)
	}
	if content != "" {
		writer.Write([]byte(content))
	} else {
		if err != nil {
			errStr := err.Error()
			writer.Write([]byte("Error: " + errStr))
			//writer.Write([]byte(Errors[500]))
		} else {
			writer.Write([]byte(Errors[500]))
		}
	}
}

// Serve starts an HTTP server providing access to registered endpoints.
func Serve(address string) error {
	return http.ListenAndServe(address, &handler{})
}
