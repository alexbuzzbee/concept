package concept

import (
	"errors"
)

// A HookFunc is a function that can be put into a hook.
type HookFunc func(param interface{}) interface{}

var hooks map[string][]HookFunc

// NewHook creates a new hook. The name should be in the format Plugin.Component.name.
func NewHook(name string) error {
	if hooks[name] != nil {
		return errors.New("hook already exists")
	}
	hooks[name] = make([]HookFunc, 1)
	return nil
}

// AddHook adds a new function to a hook.
func AddHook(name string, f HookFunc) error {
	if hooks[name] == nil {
		return errors.New("no such hook: " + name)
	}
	hooks[name] = append(hooks[name], f)
	return nil
}

// CallHook invokes all the functions in a hook, returning a slice of their return values.
func CallHook(name string, param interface{}) (returns []interface{}, err error) {
	if hooks[name] == nil {
		return nil, errors.New("no such hook: " + name)
	}
	returns = make([]interface{}, len(hooks[name]))
	for _, f := range hooks[name] {
		returns = append(returns, f(param))
	}
	return
}

func init() {
	hooks = make(map[string][]HookFunc)
}
